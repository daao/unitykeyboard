package com.kuxiong.plugins;

import com.kuxiong.plugins.AndroidKeyBoard.TextInputConnection;
import com.unity3d.player.*;

import android.content.ContextWrapper;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

public class MyUnityPlayer extends UnityPlayer
{
	public class TextInputConnection extends BaseInputConnection
	{
		public TextInputConnection(View targetView, boolean fullEditor) {
			super(targetView, fullEditor);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public boolean commitText(CharSequence text, int newCursorPosition) {
			// TODO Auto-generated method stub
			UnityPlayer.UnitySendMessage("Main", "commitText", text.toString());
			Log.i("ime","commitText:"+text);
			return super.commitText(text, newCursorPosition);
		}

		@Override
		public boolean sendKeyEvent(KeyEvent event) {
			UnityPlayer.UnitySendMessage("Main", "sendKeyEvent", event.getKeyCode()+"");
			Log.i("ime","sendKeyEvent:"+event.getKeyCode());
			// TODO Auto-generated method stub
			return super.sendKeyEvent(event);
		}
	}

	public MyUnityPlayer(ContextWrapper arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
//		this.setOnKeyListener(new OnKeyListener() {   
//				   @Override   
//				   public boolean onKey(View v, int keyCode, KeyEvent event) {
//					   UnityPlayer.UnitySendMessage("Main", "DebugKeyBoard", "setOnKeyListener"+keyCode);
//					   return true;
//				   } 
//			   });
	}
	
	@Override
	public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
		// TODO Auto-generated method stub
//		return super.onCreateInputConnection(outAttrs);
//		TextInputConnection input = new TextInputConnection(this, false);
//		outAttrs.actionLabel = null;
//		outAttrs.inputType = InputType.TYPE_NULL;
		outAttrs.actionLabel = "";
	    outAttrs.hintText = "";
	    outAttrs.initialCapsMode = 0;
	    outAttrs.initialSelEnd = outAttrs.initialSelStart = -1;
	    outAttrs.label = "";
//	    outAttrs.imeOptions = EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI;        
	    outAttrs.inputType = InputType.TYPE_NULL;    
		
		
		UnityPlayer.UnitySendMessage("Main", "DebugKeyBoard", "onCreateInputConnection");
		return new TextInputConnection(this, false);
	}
	
	@Override
	public boolean onCheckIsTextEditor ()
	{
		return true;
	}
	
}

package com.kuxiong.plugins;

import com.unity3d.player.UnityPlayer;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public class AndroidKeyBoard {
	
	public class TextInputConnection extends BaseInputConnection
	{
		public TextInputConnection(View targetView, boolean fullEditor) {
			super(targetView, fullEditor);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public boolean commitText(CharSequence text, int newCursorPosition) {
			// TODO Auto-generated method stub
			UnityPlayer.UnitySendMessage("Main", "commitText", text.toString());
			Log.i("ime","commitText:"+text);
			return super.commitText(text, newCursorPosition);
		}

		@Override
		public boolean sendKeyEvent(KeyEvent event) {
			UnityPlayer.UnitySendMessage("Main", "sendKeyEvent", event.getKeyCode()+"");
			Log.i("ime","sendKeyEvent:"+event.getKeyCode());
			// TODO Auto-generated method stub
			return super.sendKeyEvent(event);
		}
	}
	
	
	public class MyEditText extends EditText{

		@Override
		public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
			// TODO Auto-generated method stub
//			return super.onCreateInputConnection(outAttrs);
			return new TextInputConnection(this, false);
		}

		public MyEditText(Context context, AttributeSet attrs) {
			super(context, attrs);
			// TODO Auto-generated constructor stub
		}
	}
	
	
	Activity context;
	MyEditText v;
	InputMethodManager inputMethodManager;
	
	public AndroidKeyBoard()
	{
		context = UnityPlayer.currentActivity;
		inputMethodManager
		  = (InputMethodManager) UnityPlayer.currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
	}
	
	public void ShowKeyBoard()
	{
		context.runOnUiThread(new Runnable(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				v = new MyEditText(context, null);
				FrameLayout.LayoutParams params = 
						new LayoutParams
							(FrameLayout.LayoutParams.MATCH_PARENT,
									FrameLayout.LayoutParams.WRAP_CONTENT);
				params.gravity = Gravity.TOP;
////				params.setMargins(20000, 0, 0, 0);
				context.addContentView(v, params);
////				v.setClickable(false);
				v.setText("1111");
				v.setFocusableInTouchMode(true);
//				v.setCursorVisible(false);
//				v.requestFocus();
//				inputMethodManager.showSoftInput(v, InputMethodManager.SHOW_FORCED);
				inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
			}});
	}
	
	public void HideKeyBoard()
	{
		context.runOnUiThread(new Runnable(){

			@Override
			public void run() {
//				inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
//				// TODO Auto-generated method stub
				if(v!= null)
				{
					ViewGroup parent = (ViewGroup)v.getParent();
					parent.removeView(v);
					v = null;
				}
				
				inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
			}});
	}
	
}

package com.kuxiong.vglass;

import com.kuxiong.plugins.MyUnityPlayer;
import com.unity3d.player.*;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class UnityPlayerActivity extends Activity
{
	protected MyUnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code
	
	ViewTreeObserver.OnGlobalLayoutListener listener;
	
	// Setup activity layout
	@Override protected void onCreate (Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		getWindow().setFormat(PixelFormat.RGBX_8888); // <--- This makes xperia play happy

		mUnityPlayer = new MyUnityPlayer(this);
		setContentView(mUnityPlayer);
		mUnityPlayer.setFocusable(true);
		mUnityPlayer.setFocusableInTouchMode(true);
		mUnityPlayer.requestFocus();
		
		inputMethodManager
		  = (InputMethodManager) UnityPlayer.currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		
//		mChildOfContent = mUnityPlayer.getView();
		usableHeightPrevious = computeUsableHeight();
		listener = new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				possiblyResizeChildOfContent();
			}
		};	
		
	}
	
	InputMethodManager inputMethodManager;
//	private View mChildOfContent;
	private int usableHeightPrevious;
	private void possiblyResizeChildOfContent() {
        int usableHeightNow = computeUsableHeight();
//        if (usableHeightNow != usableHeightPrevious) {
        	UnityPlayer.UnitySendMessage("Main", "OnKeyBoardChange", usableHeightNow+"");
//          usableHeightPrevious = usableHeightNow;
//        }
    }
	private int computeUsableHeight() {
        Rect r = new Rect();
        mUnityPlayer.getWindowVisibleDisplayFrame(r);
        return r.bottom;
//        return (r.bottom - r.top);// 全屏模式下： return r.bottom
    }
	
	
	
	public void AddKeyBoardChangeListener(){
		mUnityPlayer.getViewTreeObserver().addOnGlobalLayoutListener(listener);
	}
	
	public void ShowKeyBoard()
	{
		UnityPlayer.UnitySendMessage("Main", "DebugKeyBoard", "ShowKeyBoard");
		this.runOnUiThread(new Runnable(){
			@Override
			public void run() {
				mUnityPlayer.requestFocus();
//				inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
				inputMethodManager.showSoftInput(getCurrentFocus(),InputMethodManager.SHOW_FORCED);
//				mUnityPlayer.getViewTreeObserver().addOnGlobalLayoutListener(listener);
			}});
	}
	
	public void RemoveKeyBoardChangeListener(){
		mUnityPlayer.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
	}
	
	public void HideKeyBoard()
	{
		UnityPlayer.UnitySendMessage("Main", "DebugKeyBoard", "HideKeyBoard");
//		InputMethodManager inputMethodManager
//		  = (InputMethodManager) UnityPlayer.currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
//		inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
		inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//		mUnityPlayer.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
	}
	
	// Quit Unity
	@Override protected void onDestroy ()
	{
		mUnityPlayer.quit();
		super.onDestroy();
	}

	// Pause Unity
	@Override protected void onPause()
	{
		super.onPause();
		mUnityPlayer.pause();
	}

	// Resume Unity
	@Override protected void onResume()
	{
		super.onResume();
		mUnityPlayer.resume();
	}

	// This ensures the layout will be correct.
	@Override public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		mUnityPlayer.configurationChanged(newConfig);
	}

	// Notify Unity of the focus change.
	@Override public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
		mUnityPlayer.windowFocusChanged(hasFocus);
	}

	// For some reason the multiple keyevent type is not supported by the ndk.
	// Force event injection by overriding dispatchKeyEvent().
	@Override public boolean dispatchKeyEvent(KeyEvent event)
	{
		if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
			return mUnityPlayer.injectEvent(event);
		return super.dispatchKeyEvent(event);
	}

	// Pass any events not handled by (unfocused) views straight to UnityPlayer
	@Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
	@Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
	@Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
	/*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }
}

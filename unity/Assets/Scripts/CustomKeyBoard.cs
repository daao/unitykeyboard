﻿using UnityEngine;
using System.Collections;
using FairyGUI;

public class CustomKeyBoard : IKeyboard
{

    #region legacy
    //    public bool done
    //    {
    //        get { return false; }
    //    }

    //    public string GetInput()
    //    {
    //        return string.Empty;
    //    }

    //    public void Open(string text, bool autocorrection, bool multiline, bool secure, bool alert, string textPlaceholder, int keyboardType, bool hideInput)
    //    {
    //        SDKMgr.Instance.ShowKeyBoard();
    ////        using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
    ////        {
    ////            AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity");
    ////                //.Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");
    ////            View.Call("ShowKeyBoard");
    ////            //using (AndroidJavaObject Rct = new AndroidJavaObject("android.graphics.Rect"))
    ////            //{
    ////
    ////            //    View.Call("getWindowVisibleDisplayFrame", Rct);
    ////            //    ///the height is window visiable height which not hide by keybord
    ////            //    DebugEx.Log(" => Screen.height  " + Screen.height + " => height " + Rct.Call<int>("height"));
    ////
    ////            //    return Screen.height - Rct.Call<int>("height");
    ////            //}
    ////        }

    //        //if (_keyboard != null)
    //        //    return;

    //        //UnityEngine.TouchScreenKeyboard.hideInput = hideInput;
    //        //_keyboard = UnityEngine.TouchScreenKeyboard.Open(text, (TouchScreenKeyboardType)keyboardType, autocorrection, multiline, secure, alert, textPlaceholder);
    //    }
    //    public void Close()
    //    {
    //        SDKMgr.Instance.HideKeyBoard();
    ////        using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
    ////        {
    ////            AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity");
    ////            //.Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");
    ////            View.Call("HideKeyBoard");
    ////            //using (AndroidJavaObject Rct = new AndroidJavaObject("android.graphics.Rect"))
    ////            //{
    ////
    ////            //    View.Call("getWindowVisibleDisplayFrame", Rct);
    ////            //    ///the height is window visiable height which not hide by keybord
    ////            //    DebugEx.Log(" => Screen.height  " + Screen.height + " => height " + Rct.Call<int>("height"));
    ////
    ////            //    return Screen.height - Rct.Call<int>("height");
    ////            //}
    ////        }


    //        //if (_keyboard != null)
    //        //{
    //        //    _keyboard.active = false;
    //        //    _keyboard = null;
    //        //}
    //    }
    #endregion

    public CustomKeyBoard()
    {
        this.done = true;
    }

    public bool done { get; set; }


    public bool supportsCaret
    {
        get { return true; }
    }

    public string GetInput()
    {
        return string.Empty;
    }

    public void Open(string text, bool autocorrection, bool multiline, bool secure, bool alert, string textPlaceholder, int keyboardType, bool hideInput)
    {
		DebugEx.Log("ShowKeyBoard");

		//throw new System.NotImplementedException();
		SDKMgr.Instance.ShowKeyBoard();

		this.done = false;
    }

    public void Close()
    {
        DebugEx.Log("HideKeyBoard");

        //throw new System.NotImplementedException();
        SDKMgr.Instance.HideKeyBoard();
        this.done = true;
    }
}

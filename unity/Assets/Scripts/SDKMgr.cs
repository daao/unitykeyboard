﻿using UnityEngine;
using System.Collections;

public class SDKMgr : Singleton<SDKMgr>
{


    public AndroidJavaObject androidKeyBoard; ///only
    
    public void Init()
	{
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                androidKeyBoard = null;
                break;
            case RuntimePlatform.WindowsEditor:
                Debug.Log("Sdk Connector start");
                break;
            default:
                break;
        }
	}


	public void SettingKeyBoard()
	{
		switch (Application.platform)
		{
			case RuntimePlatform.Android:
				using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity");
					View.Call("AddKeyBoardChangeListener");
				}
				break;
		}
	}
	public void UnSettingKeyBoard()
	{
		switch (Application.platform)
		{
			case RuntimePlatform.Android:
				using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity");
					View.Call("RemoveKeyBoardChangeListener");
				}
				break;
		}
		
	}


    public void ShowKeyBoard() 
    {
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
//                androidKeyBoard = new AndroidJavaObject("com.kuxiong.plugins.AndroidKeyBoard");
//                androidKeyBoard.Call("ShowKeyBoard");

				using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity");
					View.Call("ShowKeyBoard");
//					DebugEx.Log(" => Screen.height  " + Screen.height + " => height " + Rct.Call<int>("height"));
				}
                break;
            case RuntimePlatform.IPhonePlayer:
                break;
            default:
                break;
        }   
    }
    
    public void HideKeyBoard() 
    {
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
				using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity");
					View.Call("HideKeyBoard");
					//					DebugEx.Log(" => Screen.height  " + Screen.height + " => height " + Rct.Call<int>("height"));
				}
//                if (androidKeyBoard != null)
//                {
//                    androidKeyBoard.Call("HideKeyBoard");
//                    androidKeyBoard.Dispose();
//                    androidKeyBoard = null;
//                }
                break;
            case RuntimePlatform.IPhonePlayer:
                break;
            default:
                break;
        }    
    }

    public int GetKeyboardHeight()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                //http://blog.csdn.net/ccpat/article/details/55224475
                //对系统状态栏高度，获取一个非全屏，且窗口的LayoutParams的height设置为WindowManager.LayoutParams.MATCH_PARENT的窗口可视区域大小，其top值就是状态栏的高度。
                //对系统软键盘，获取一个高度是MATCH_PARENT的窗口在软键盘显示和隐藏两种不同状态下的可视区域大小，将bottom值相减就可以得到软键盘的高度。
                //对系统系统虚拟按键栏，获取一个高度是MATCH_PARENT的窗口在虚拟按键显示和隐藏两种不同状态下的可视区域大小，将bottom值相减就可以得到虚拟按键的高度。

                using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                {
                    AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");

                    using (AndroidJavaObject Rct = new AndroidJavaObject("android.graphics.Rect"))
                    {

                        View.Call("getWindowVisibleDisplayFrame", Rct);
                        ///the height is window visiable height which not hide by keybord
                        DebugEx.Log(" => Screen.height  " + Screen.height + " => height " + Rct.Call<int>("height"));
                        
                        return Screen.height - Rct.Call<int>("height");
                    }
                }
            case RuntimePlatform.IPhonePlayer:
                return (int)TouchScreenKeyboard.area.height;
            case RuntimePlatform.WindowsEditor:
                return (int)(0.3f * Screen.height);
            default:
                return 0;
        }
    }
}

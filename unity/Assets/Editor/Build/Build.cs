﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Build 
{
	[MenuItem("Build/BuildAndroidProject")]
	public static void BuildAndroidProject()
    {
//		PlayerSettings.companyName = "kuxiong";
//		PlayerSettings.productName = "vglass";
//		PlayerSettings.bundleIdentifier = "com.kuxiong.vglass";



        string[] commadnLineArgs = System.Environment.GetCommandLineArgs();
        var Android_Project_OutPutPath
            = commadnLineArgs[commadnLineArgs.Length - 1];
        ///must add this line to override this project
        if (Directory.Exists(Android_Project_OutPutPath)) 
            Directory.Delete(Android_Project_OutPutPath, true);

        var buildOpeion =
            BuildOptions.AcceptExternalModificationsToPlayer
                    | BuildOptions.StrictMode; 
	
	   var secens = GetBuildScenes();
	   if(secens==null || secens.Length <= 0)
			throw new System.Exception(" are you sure this scens zero you want export, fuck !");

       var result = BuildPipeline.BuildPlayer(
			secens,
                Android_Project_OutPutPath,
                    BuildTarget.Android,
                        buildOpeion);




        ///some time you not set jdk or android it will fail to build
        ///but jekins wikk build successly
        ///of soucre sript compile wrong also build wrong
        ///also BuildOptions.StrictMode no use 
        UnityEngine.Debug.Log(" output log : =>  Android_Project_OutPutPath  " + Android_Project_OutPutPath);
        UnityEngine.Debug.Log(" output log : => BuildPipeline.BuildPlayer  " + ":" + result + ":");

        //2017-5-29 append
        //may be consider set via script 
            //like android Path / java Path / PackageName
    //https://effectiveunity.com/articles/how-to-set-the-android-sdk-path-via-scripting-in-unity/

        //  If you want to return an error from the commandline process 
        //      you can either throw an exception which will cause Unity to exit with 1
        //          or else call EditorApplication.Exit with a non-zero code. 
        //  If you want to pass parameters you can add them to the command line 
        //      and retrieve them inside the method using System.Environment.GetCommandLineArgs.
        if (!string.IsNullOrEmpty(result)) throw new System.Exception(" BuildAndroidProject wrong cause : " + result);
	}

	static string[] GetBuildScenes()
	{
		
		List<string> names = new List<string>();
		
		foreach(EditorBuildSettingsScene e in EditorBuildSettings.scenes)
			
		{
			
			if(e==null)
				
				continue;
			
			if(e.enabled)
				
				names.Add(e.path);
			
		}
		
		return names.ToArray();	
	}
	

}

﻿using UnityEngine;
using System.Collections.Generic;
using FairyGUI;
using DG.Tweening;

public class Main : MonoBehaviour {

    void Awake()
    {
#if UNITY_WEBPLAYER || UNITY_WEBGL || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR
		CopyPastePatch.Apply();
#endif

#if UNITY_5
		//Use the font names directly
		UIConfig.defaultFont = "Microsoft YaHei";
#else
        //Need to put a ttf file into Resources folder. Here is the file name of the ttf file.
        UIConfig.defaultFont = "afont";
#endif
        UIPackage.AddPackage("MainChat");
        GRoot.inst.SetContentScaleFactor(720, 1280, UIContentScaler.ScreenMatchMode.MatchHeight);
		Stage.inst.keyboard = new CustomKeyBoard();
    }
    private GComponent _mainView;



	GComponent inputCom;
	GTextInput input;
    


	void OnEnable()
	{
		SDKMgr.Instance.SettingKeyBoard();
	}

	void OnDisable()
	{
		SDKMgr.Instance.UnSettingKeyBoard();
	}

	void Start()
    {
		UnityEngine.Debug.Log("unity start");

        Application.targetFrameRate = 60;
        

        _mainView = FairyGUI.UIPackage.CreateObject("MainChat", "MainChatPanel").asCom;
        _mainView.SetSize(FairyGUI.GRoot.inst.width, FairyGUI.GRoot.inst.height);
        _mainView.AddRelation(FairyGUI.GRoot.inst, FairyGUI.RelationType.Size);
        FairyGUI.GRoot.inst.AddChild(this._mainView);


		this.inputCom = _mainView
            .GetChild("n43").asCom
            .GetChild("n6").asCom;
        //inputCom.Center();

		Stage.inst.focus = null;

		this.input = inputCom 
            .GetChild("n13").asTextInput;
		input.onClick.Add(OnClick);
		input.onFocusIn.Set(OnFocusIn);
        input.onFocusOut.Add(OnFocusOut);
		input.onKeyDown.Add(OnKeyDown);

        DebugEx.Log(" Stage.keyboardInput " + Stage.keyboardInput);
    }

	public void OnKeyDown(EventContext context)
	{
		DebugEx.Log("OnKeyDown" + context.inputEvent.keyCode);
	}

	public void OnClick() 
	{
		DebugEx.Log("OnClick");
		Stage.inst.keyboard.Open("", false,  false,
			false, false, null, 0, false);
	}

    public void OnFocusIn() 
    {
        //SDKMgr.Instance.ShowKeyBoard();
        DebugEx.Log("OnFocusIn");
    }
    public void OnFocusOut() 
    {
        //SDKMgr.Instance.HideKeyBoard();
        DebugEx.Log("OnFocusOut");
    }

    void OnGUI()
    {
		if (GUI.Button(new Rect(10, 500, 150, 100), "I am a button"))
			OnKeyBoardChange(1161+"");
		
		if (GUI.Button(new Rect(10, 800, 150, 100), "I am a button"))
			OnKeyBoardChange(1920+"");

        LogManager.Instance.Show();
    }


	public void OnKeyBoardChange(string visiableWindowHight)
	{
		Vector2 screenPoint = new Vector2(0, int.Parse(visiableWindowHight));



//		int realKeyBoardHight = Screen.height - int.Parse(visiableWindowHight);

		this.inputCom.position = this.inputCom.parent.GlobalToLocal(screenPoint);


		///////////////
		///cal input offset
		Vector2 offset = this.input.TransformPoint(
			new Vector2(0, this.input.height * 1.5f),
			this.inputCom);
		DebugEx.Log("OnKeyBoardChange:" + visiableWindowHight
			+ " this.input.height " + this.input.height
			+ " screenPoint " + screenPoint
			+ " offset " + offset);
		///inpuf offset to global remine * -1
		var global = this.inputCom.LocalToGlobal(new Vector2(0, -offset.y));
		this.inputCom.position = this.inputCom.parent.GlobalToLocal(global);
	}


	public void DebugKeyBoard(string content)
	{
		DebugEx.Log("DebugKeyBoard:" + content + ":");
	}

    public void commitText(string context)
    {
        DebugEx.Log("commitText:" + context + ":");

		if(string.IsNullOrEmpty(context)){
			return;
		}

		Stage.inst.InputString(context);
    }
    public void sendKeyEvent(string keyCode)
    {
        DebugEx.Log("sendKeyEvent:" + keyCode);
    }

}
